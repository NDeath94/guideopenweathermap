﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class WeatherData : MonoBehaviour {

    [System.Serializable]
    public class Coordinates
    {
        public float lon;
        public float lat;
    }

    [System.Serializable]
    public class WeatherMain
    {
        public float temp;
        public int pressure;
        public int humidity;
    }

    [System.Serializable]
    public class Wind
    {
        public float speed;
        public int deg;
    }

    [System.Serializable]
    public class Clouds
    {
        public string all;
    }

    [System.Serializable]
    public class WeatherDescr
    {
        public string main;
        public string description;
        public string icon;
    }

    [System.Serializable]
    public class Sys
    {
        public string country;
    }

    [System.Serializable]
    public class WeatherInfo
    {
        public Coordinates coord;
        public WeatherDescr[] weather;
        public Wind wind;
        public Clouds clouds;
        public WeatherMain main;
        public Sys sys;
        public string name;   
    }
    
    
    

    public Text tempText;
   
    private static string cityID = "2615876";    // Insert city ID
    private static string apiKey = "15e12369cb158ee3a17814eb9e1bba94";    // Insert API key

    private static string url = "http://api.openweathermap.org/data/2.5/weather?id=" + cityID + "&units=metric" + "&APPID=" + apiKey;

    public WeatherInfo wData;

    IEnumerator  Start () {
        using (WWW www = new WWW(url))
        {
            yield return www;

            wData = JsonUtility.FromJson<WeatherInfo>(www.text);

            Debug.Log(www.text);

            tempText.text = "Data for " + wData.name + ":\n";
            tempText.text += wData.name + "s landkode er " + wData.sys.country + ".\n";
            tempText.text += "Temperaturen er " + wData.main.temp + " grader.\n";
            tempText.text += "Fugtbarheden er " + wData.main.humidity + "%.\n";
            if (wData.main.humidity > 65)
            {
                tempText.text += "Det er pisse klamt og fugtigt. Mine hænder er helt fedtede..\n";
            }
            tempText.text += "Vind hastigheden " + wData.wind.speed + " m/s.\n";
            tempText.text += "Det er " + wData.clouds.all + "% overskyet.\n";
        }
	}
}
